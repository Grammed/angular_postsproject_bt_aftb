import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../models/User';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  
  user: User = {
    firstName: '',
    lastName: '',
    email: ''
  }

//@ViewChild queries the referenced template and injects them into this component.
  @ViewChild('userForm') form: any;
  users: User[];
  showExtended: boolean = true;
  loaded: boolean = false;
  enableAdd: boolean = false;
  showUserForm: boolean = false;

  data: any;

  constructor(private userService: UserService) { }

  ngOnInit(): void {

    this.userService.getData().subscribe(dataSpurt => {
      //console.log(dataSpurt);
    })

    this.userService.getUsers().subscribe(staticSpurt => {
      this.users = staticSpurt;
      this.loaded = true;
    });

    console.log(this.users.length)
 }
  
onSubmit({value, valid}: {value: User, valid: boolean}) {
  if (!valid) {
    console.log('Form is not valid');
  } else {
    value.isActive = true;
    value.registered = new Date();
    value.hide = true;

    this.userService.addUser(value);

  }
  this.form.reset();
  }




}
