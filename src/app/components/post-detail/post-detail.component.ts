import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PostService} from '../../services/post.service';
import {Post} from '../../models/Post';

@Component({
  selector: 'app-postdetail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  post: Post;

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
) {}

  ngOnInit(): void {
    // Step 1 get id that's passed into url via a snapshot when the url is activated post-detail/id
    const id = +this.route.snapshot.paramMap.get('id');

    // Step 3 (step 2 is make the observable in the service:
    this.postService.getPost(id).subscribe(post => this.post = post);

  }

}
