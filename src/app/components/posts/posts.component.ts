import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { Post } from '../../models/Post'

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})


export class PostsComponent implements OnInit {
  posts: Post[];
  currentPost: Post = {
    id: 0,
    title: '',
    body: ''
  }
  isEdit: boolean = false;



  constructor(private postService: PostService) {
       }





  ngOnInit(): void {
      this.postService.getPosts().subscribe(arrayOfPosts => {
      console.log(arrayOfPosts);
      this.posts = arrayOfPosts;
    });

  }

  onNewPost(postEvent: Post) {
    this.posts.unshift(postEvent);
  }


  editPost(ngIfpost: Post): void {
    this.currentPost = ngIfpost;
    this.isEdit = true;
  }

  onUpdatedPost(post: Post): void {
    this.posts.forEach((cur, index) => {
      if(post.id === cur.id) {
        this.posts.splice(index, 1);
        this.posts.unshift(post);
        this.isEdit = false;
        this.currentPost = {
          id: 0,
          title: '',
          body: ''
        };
      }
    });
  }

  removePost(post: Post): void {
    if(confirm ('Are you sure?')) {
      this.postService.removePost(post.id).subscribe(() => {
        this.posts.forEach((cur, index) => {
          if (post.id === cur.id) {
            this.posts.splice(index, 1);
          }
        });
      });
    }
  }
}
