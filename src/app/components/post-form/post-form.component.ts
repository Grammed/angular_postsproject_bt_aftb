import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Post } from 'src/app/models/Post';
import { PostService } from 'src/app/services/post.service';


@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})


export class PostFormComponent implements OnInit {
  
  @Output() updatedPost: EventEmitter<Post> = new EventEmitter();
  @Output() newPost: EventEmitter<Post> = new EventEmitter();
  // without this input the: <app-post-form [currentPost]="currentPost" is not recognised. Originates from button click editPost()
  @Input() currentPost: Post;
  @Input() isEdit: boolean;
  constructor(private postService: PostService) { }

  ngOnInit(): void {
  }

addPost(title, body) {
  if(!title || !body) {
    alert('Please add post');
  }else {
    this.postService.savePost({title, body} as Post)
    .subscribe(post => {
      //@output() new post to posts component for display
      this.newPost.emit(post);
    });
  }
}

updatePost() {
   
  this.postService
  .updatePost(this.currentPost).subscribe(post => {
    console.log(post);
    this.isEdit = false;
    //this emits to the post-form component html tag
    this.updatedPost.emit(post);
  });
}


}
