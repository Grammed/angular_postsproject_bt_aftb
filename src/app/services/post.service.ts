import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// The http client returns an observable that needs to be imported too:
import { Observable } from 'rxjs';
import {Post} from '../models/Post';

// HttpHeaders object required for POST request-->
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})



export class PostService {
  postsUrl = 'https://jsonplaceholder.typicode.com/posts';




  constructor(private http: HttpClient ) { }

  // READ - GET
  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postsUrl);
  }
  // CREATE - POST
  savePost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postsUrl, post, httpOptions);
  }
  // UPDATE - PUT
  updatePost(post: Post): Observable<Post> {
    const url = `${this.postsUrl}/${post.id}`;
    return this.http.put<Post>(url, post, httpOptions);
  }

  // DELETE
  removePost(post: Post | number): Observable<Post> {
    const id = typeof post === 'number' ? post : post.id;
    const url = `${this.postsUrl}/${id}`;
    return this.http.delete<Post>(url, httpOptions);
  }

  getPost(id: number): Observable<Post> {
    const url = `${this.postsUrl}/${id}`;
    // just fetching data so no need for post or httpOptions
    return this.http.get<Post>(url);
  }
}
