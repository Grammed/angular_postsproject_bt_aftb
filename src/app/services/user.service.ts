import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class UserService {

users: User[];
data: Observable<any>;


    constructor() {

 this.users = [
  {
      firstName: 'Jonn',
      lastName: 'Dohe',
      email: 'some@Email.com',
      isActive: false,
      registered: new Date('3/2/2018 10:08:49'),
      hide: true 
    },   {
      firstName: 'Jinn',
      lastName: 'Doie',
      email: 'anotherfake@email.com',
      isActive: true,
      registered: new Date('12/9/2018 19:38:20'),
      hide: true
    },   {
      firstName: 'Smas',
      lastName: 'Dodas',
      email: 'thisis@email.com',
      isActive: false,
      registered: new Date('7/23/2020 08:30:00'),
      hide: true 
    } 
  ];
   }

   //Updated
   getUsers(): Observable<User[]> {
     console.log('Fetching users from service');
     return of(this.users);
   }

   addUser(user: User) {
     this.users.unshift(user);
   }


   getData(){
     this.data = new Observable(observer => {

      setTimeout(() => {
        observer.next(1);
      }, 1000);

      setTimeout(() => {
        observer.next(2);
      }, 2000);

      setTimeout(() => {
        observer.next(3);
      }, 3000);

      setTimeout(() => {
        observer.next({name: 'Brad'});
      }, 4000);

     });

     return this.data;
   }


  }
