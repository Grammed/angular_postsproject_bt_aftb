// the endpoint had added userId since the tutorial was made.

export interface Post {
  // userId: number,
  id: number;
  title: string;
  body: string;
}
